﻿using Masd.EAutoService.MechanicApp.Model;
using Masd.EAutoService.MechanicApp.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masd.EAutoService.MechanicApp.Controller
{
    public partial class Controller : PropertyContainerBase, IController
    {
        public IModel Model { get; private set; }

        public Controller(IEventDispatcher dispatcher, IModel model) : base(dispatcher)
        {
            this.Model = model;

            this.ShowOrdersCommand = new ControllerCommand(this.ShowOrders);

            this.ShowOrderCommand = new ControllerCommand(this.ShowOrder);

            this.UpdateOrderStatusCommand = new ControllerCommand(() => this.UpdateOrderStatus());

            this.ShowServicesCommand = new ControllerCommand(this.ShowServices);

            this.AddServicesToOrderCommand = new ControllerCommand(() => this.AddServicesToOrder());
        }
    }
}
