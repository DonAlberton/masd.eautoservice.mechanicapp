﻿using System.Windows.Input;

namespace Masd.EAutoService.MechanicApp.Controller
{
    public partial class Controller : IController
    {
        public ApplicationState CurrentState
        {
            get { return this.currentState; }
            set
            {
                this.currentState = value;

                this.RaisePropertyChanged("CurrentState");
            }
        }
        private ApplicationState currentState = ApplicationState.List;


        public ICommand ShowOrdersCommand { get; private set; }
        public ICommand ShowOrderCommand { get; private set; }
        public ICommand UpdateOrderStatusCommand { get; private set; }
        public ICommand ShowServicesCommand { get; private set; }
        public ICommand AddServicesToOrderCommand { get; private set; }

        public async Task ShowOrdersAsync()
        {
            await Task.Run(() => this.ShowOrders());
            
        }
        public async Task ShowOrderAsync()
        {
            await Task.Run(() => this.ShowOrder());
        }
        public async Task UpdateOrderStatusAsync()
        {
            await Task.Run(()=> this.UpdateOrderStatus());
            await Task.Run(() => this.ShowOrders());

        }
        public async Task ShowServicesAsync()
        {
            await Task.Run(() => this.ShowServices());
        }
        public async Task AddServicesToOrderAsync()
        {
            await Task.Run(() => this.AddServicesToOrder());
            await Task.Run(() => this.ShowOrders());
        }

        private void ShowOrders()
        {
            this.Model.LoadOrdersList();
        }
        private void ShowOrder()
        {
            this.Model.LoadSelectedOrder();
        }
        private void UpdateOrderStatus()
        {
            this.Model.ChangeOrderStatus();
        }
        private void ShowServices()
        {
            this.Model.LoadServicesList();
        }

        private void AddServicesToOrder()
        {
            this.Model.AddServices();
        }
    }
}