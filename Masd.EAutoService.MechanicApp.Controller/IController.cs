﻿using Masd.EAutoService.MechanicApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Masd.EAutoService.MechanicApp.Controller
{
    public interface IController : INotifyPropertyChanged
    {
        IModel Model { get; }

        ApplicationState CurrentState { get; }

        ICommand ShowOrdersCommand { get; }
        ICommand ShowOrderCommand { get; }
        ICommand UpdateOrderStatusCommand { get; }
        ICommand ShowServicesCommand { get; }
        ICommand AddServicesToOrderCommand {get; }

        Task ShowOrdersAsync();
        Task ShowOrderAsync();
        Task UpdateOrderStatusAsync();
        Task ShowServicesAsync();
        Task AddServicesToOrderAsync();
    }
}
