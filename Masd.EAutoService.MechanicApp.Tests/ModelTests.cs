﻿namespace Masd.EAutoService.MechanicApp.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Masd.EAutoService.MechanicApp.Controller;
    using Masd.EAutoService.MechanicApp.Model;
    using Masd.EAutoService.MechanicApp.Utilities;

    [TestClass]
    public class ModelTests
    {
        [TestMethod]
        public void LoadOrdersList_ReadFromOrdersArray_ThereIsTwelveOrders()
        {
            IModel model = new Model(new EmptyEventDispatcher());

            model.LoadOrdersList();
            int expectedCount = 12;
            int actualCount = model.OrdersList.Count;

            Assert.AreEqual(expectedCount, actualCount, "Order count should be {0} and not {1}", expectedCount, actualCount);


        }
        [TestMethod]
        public void LoadOrdersList_ReadFromOrdersArray_CheckMechanicNameForOrderId()
        {
            IModel model = new Model(new EmptyEventDispatcher());

            model.LoadOrdersList();
            string expectedMechanicName = "Jan";
            string actualMechanicName = model.OrdersList[2].MechanicName!;

            Assert.AreEqual(expectedMechanicName, actualMechanicName, "Mechanic name should be {0} and not {1}", expectedMechanicName, actualMechanicName);
        }
        [TestMethod]
        public void LoadOrdersList_ReadFromOrdersArray_CheckMechanicSurnameForOrderId()
        {
            IModel model = new Model(new EmptyEventDispatcher());

            model.LoadOrdersList();
            string expectedMechanicSurname = "Samochodowy";
            string actualMechanicSurname = model.OrdersList[6].MechanicSurname!;

            Assert.AreEqual(expectedMechanicSurname, actualMechanicSurname, "Mechanic Surname should be {0} and not {1}", expectedMechanicSurname, actualMechanicSurname);
        }
        [TestMethod]
        public void LoadOrdersList_ReadFromOrdersArray_CheckCustomerNameForOrderId()
        {
            IModel model = new Model(new EmptyEventDispatcher());

            model.LoadOrdersList();
            string expectedCustomerSurname = "Michal";
            string actualCustomerSurname = model.OrdersList[3].CustomerName!;

            Assert.AreEqual(expectedCustomerSurname, actualCustomerSurname, "Customer name should be {0} and not {1}", expectedCustomerSurname, actualCustomerSurname);
        }


        [TestMethod]
        public void LoadServicesList_ReadFromServicesArray_ThereIsFoureOrders()
        {
            IModel model = new Model(new EmptyEventDispatcher());
            model.LoadServicesList();
            int expectedServicesListLength = 5;
            int actualServicesListLength = model.ServicesList.Count;

            Assert.AreEqual(expectedServicesListLength, actualServicesListLength, "Customer name should be {0} and not {1}", expectedServicesListLength, actualServicesListLength);
        }
    }
}
