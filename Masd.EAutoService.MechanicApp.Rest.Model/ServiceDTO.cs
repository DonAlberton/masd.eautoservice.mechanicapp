﻿namespace Masd.EAutoService.MechanicApp.Rest.Model
{
    public class ServiceDTO
    {
        public int ServiceId { get; set; }
        public string? Name { get; set; }
        public double Price { get; set; }

        public override string ToString()
        {
            return "ID: " + ServiceId + ", service: " + Name + ", cost: " + Price;
        }
    }


}