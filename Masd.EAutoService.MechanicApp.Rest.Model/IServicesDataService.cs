﻿namespace Masd.EAutoService.MechanicApp.Rest.Model
{
    public interface IServicesDataService
    {
        ServiceDTO GetService(int id);
        ServiceDTO[] GetServices();
    }
}
