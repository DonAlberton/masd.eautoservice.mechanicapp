﻿namespace Masd.EAutoService.MechanicApp.Model
{
    using Masd.EAutoService.MechanicApp.Client;
    using Masd.EAutoService.MechanicApp.Rest.Client;
    using Masd.EAutoService.MechanicApp.Rest.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Serialization;

    public partial class Model : IOperations
    {
#if DEBUG
        public OrdersAppServiceClient client = new OrdersAppServiceClient();
#else
        public MockOrdersAppServiceClient client = new MockOrdersAppServiceClient();
#endif

        public void LoadOrdersList()
        {
            this.LoadOrdersListTask();
        }

        private void LoadOrdersListTask()
        {
            try
            {
                OrderAppDTO[] orders = client.GetOrders();
                if (orders != null && orders.Length <= 0)
                {
                    Console.WriteLine("Orders not leaded correctly");
                }
                this.OrdersList = orders!.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void LoadSelectedOrder()
        {
            this.LoadSelectedOrderTask();
        }

        private void LoadSelectedOrderTask()
        {
            try
            {
                OrderAppDTO[] order = new OrderAppDTO[] { client.GetOrder(this.OrderId) };
                if (order[0] == null)
                {
                    throw new Exception("Invalid ID");
                }

                this.OrdersList = order.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        public void ChangeOrderStatus()
        {
            this.ChangeOrderStatusTask();
        }

        private void ChangeOrderStatusTask()
        {
            try
            {
                if (OrderToChange == null)
                {
                    Console.WriteLine("Order is not selected");
                }
                client.EditOrder(SelectedOrder[0]!.Id, !SelectedOrder[0].IsFinished);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        public void LoadServicesList()
        {
            this.LoadServicesListTask();
        }
        private void LoadServicesListTask()
        {
            try
            {
                ServiceDTO[] servicesList = client.GetServices();

                this.ServicesList = servicesList.ToList();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void AddServices()
        {
            this.AddServicesTask();
        }

        private void AddServicesTask()
        {
            try
            {
                if (ServicesList.Count == null || SelectedOrder[0] == null)
                {
                    Console.WriteLine("Service or order is not choosen");
                }
                client.AddServicesToOrder(SelectedOrder[0]!.Id, ServicesIdList);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


    }
}
