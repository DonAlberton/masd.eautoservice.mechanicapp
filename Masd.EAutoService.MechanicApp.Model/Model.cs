﻿namespace Masd.EAutoService.MechanicApp.Model
{
  using Masd.EAutoService.MechanicApp.Utilities;
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;


  public partial class Model : PropertyContainerBase, IModel
  {
    public Model( IEventDispatcher dispatcher ) : base( dispatcher )
    {
    }
  }
}
