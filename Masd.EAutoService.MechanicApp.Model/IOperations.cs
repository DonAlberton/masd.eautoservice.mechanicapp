﻿namespace Masd.EAutoService.MechanicApp.Model
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;

  public interface IOperations
  {

    void LoadOrdersList();

    void LoadSelectedOrder();

    void ChangeOrderStatus();

    void LoadServicesList();

    void AddServices();
  }
}
