﻿namespace Masd.EAutoService.MechanicApp.Model
{
    using Masd.EAutoService.MechanicApp.Rest.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public partial class Model : IData
    {

        private List<OrderAppDTO> ordersList = new List<OrderAppDTO>();

        public List<OrderAppDTO> OrdersList
        {
            get { return this.ordersList; }
            set
            {
                this.ordersList = value;
                this.RaisePropertyChanged("OrdersList");
            }
        }
        public int OrderId
        {
            get { return this.orderId; }
            set
            {
                this.orderId = value;

                this.RaisePropertyChanged("OrderId");
            }
        }
        private int orderId;

        public IList<OrderAppDTO> SelectedOrder
        {
            get { return this.selectedOrder; }
            set
            {
                this.selectedOrder = value;

                this.RaisePropertyChanged("SelectedOrder");
            }
        }
        private IList<OrderAppDTO> selectedOrder;

        private OrderAppDTO orderToChange;
        public OrderAppDTO OrderToChange
        {
            get { return this.orderToChange;}
            set 
            { 
                this.orderToChange = value;
                this.RaisePropertyChanged("OrderToChange");
            }
        }      


        private List<ServiceDTO> servicesList;
        public List<ServiceDTO> ServicesList
        {
            get { return this.servicesList; }
            set
            {
                this.servicesList = value;
                this.RaisePropertyChanged("ServicesList");
            }
        }

        private List<int> servicesIdList;
        public List<int> ServicesIdList
        {
            get { return this.servicesIdList; }
            set
            {
                this.servicesIdList = value;
                this.RaisePropertyChanged("ServicesIdList");
            }
        }
    }
}
