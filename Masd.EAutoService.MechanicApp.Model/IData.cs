﻿namespace Masd.EAutoService.MechanicApp.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using System.ComponentModel;
    using Masd.EAutoService.MechanicApp.Rest.Model;

    public interface IData : INotifyPropertyChanged
    {
        int OrderId { get; set; }   

        List<OrderAppDTO> OrdersList { get; set; }

        IList<OrderAppDTO> SelectedOrder { get; set; }

        OrderAppDTO OrderToChange { get; set; }


        List<ServiceDTO> ServicesList { get; set; } 

        List<int> ServicesIdList { get; set; }  
    }
}
