﻿namespace Masd.EAutoService.MechanicApp.Rest.Client
{
    using System.Net.Http;
    using System.Text.Json;
    using Masd.EAutoService.MechanicApp.Rest.Model;

    public class OrdersAppServiceClient : IOrdersAppService
    {


        private static readonly HttpClient httpClient = new HttpClient();
        private static readonly string ipAddress = "192.168.50.111";
        public static int port = 80;

        /*private static readonly string ipAddress = "localhost";
        public static int port = 8091;*/
        public OrderAppDTO[] GetOrders()
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersApp/GetOrders";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            OrderAppDTO[] ordersData = ConvertJson(jsonResponseContent);

            return ordersData;
        }
        public OrderAppDTO GetOrder(int id)
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersApp/GetOrder?id={id}";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            OrderAppDTO mechanicsData = ConvertJsonObject(jsonResponseContent);

            return mechanicsData;
        }

        private async Task<string> CallWebService(HttpMethod httpMethod, string webServiceUrl)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }

        private OrderAppDTO[] ConvertJson(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            OrderAppDTO[] mechanicsData = JsonSerializer.Deserialize<OrderAppDTO[]>(json, options);

            return mechanicsData;
        }

        private OrderAppDTO ConvertJsonObject(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            OrderAppDTO mechanicsData = JsonSerializer.Deserialize<OrderAppDTO>(json, options);

            return mechanicsData;
        }
        public void EditOrder(int id, bool isFinished)
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersApp/EditOrder?id={id}&isFinished={isFinished}";

            Task<string> webServiceCall = CallWebService(HttpMethod.Put, webServiceUrl);

            webServiceCall.Wait();

        }

        public void AddServicesToOrder(int id, List<int> services)
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersApp/AddServicesToOrder?id={id}";
            Task<string> webServiceCall = CallWebServiceWithBody(HttpMethod.Put, webServiceUrl, services);

            webServiceCall.Wait();
        }

        private async Task<string> CallWebServiceWithBody(HttpMethod httpMethod, string webServiceUrl, List<int> body)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);
            string requestBody = JsonSerializer.Serialize(body);
            HttpResponseMessage httpResponseMessage = await httpClient.PutAsync(webServiceUrl, new StringContent(requestBody, System.Text.Encoding.UTF8, "application/json"));

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }

        public ServiceDTO[] GetServices()
        {
            string webServiceUrl = String.Format("http://{0}:{1}/OrdersApp/GetServices", ipAddress, port);

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            ServiceDTO[] servicesData = ConvertJsonServices(jsonResponseContent);

            return servicesData;

        }

        private ServiceDTO[] ConvertJsonServices(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            ServiceDTO[] servicesData = JsonSerializer.Deserialize<ServiceDTO[]>(json, options)!;

            return servicesData;
        }
    }
}
