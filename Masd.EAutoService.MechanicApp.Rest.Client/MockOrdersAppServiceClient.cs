﻿namespace Masd.EAutoService.MechanicApp.Client
{
    using Masd.EAutoService.MechanicApp.Rest.Model;


    public class MockOrdersAppServiceClient : IOrdersAppService
    {

        public static OrderAppDTO[] ordersDTO = new OrderAppDTO[]
        {
            new OrderAppDTO { Id=1, MechanicName="Robert", MechanicSurname="Winnicki", CustomerName="Michał", CustomerSurname="Wyszynski", IsFinished=true, ServiceList = new List<String>{"Oil change", "Tires change" } },
            new OrderAppDTO { Id=2, MechanicName="Jan", MechanicSurname="Merloni", CustomerName="Adam", CustomerSurname="Rybalko", IsFinished=false, ServiceList = new List<string>{"Tires change"} },
            new OrderAppDTO { Id=3, MechanicName="Jan", MechanicSurname="Merloni", CustomerName = "Michael", CustomerSurname = "Olivier", IsFinished=true, ServiceList = new List<string>{"Tires change"} },
            new OrderAppDTO { Id=4, MechanicName="Robert", MechanicSurname="Mike", CustomerName = "Michal", CustomerSurname = "Mike", IsFinished=false, ServiceList = new List<string>{ "Tires change, Filter Change" } },
            new OrderAppDTO { Id=5, MechanicName="Scot", MechanicSurname="McTomminay", CustomerName = "Scot", CustomerSurname = "McTomminay", IsFinished=true, ServiceList = new List<string>{ "Breaks Change" } },
            new OrderAppDTO { Id=6, MechanicName="Jacob", MechanicSurname="Samochodowy", CustomerName="Jacob", CustomerSurname="Smith", IsFinished=false, ServiceList = new List<string>{"Tires change"} },
            new OrderAppDTO { Id=7, MechanicName="Jacob", MechanicSurname="Samochodowy", CustomerName="Ricardo", CustomerSurname="Sanchez", IsFinished=true, ServiceList = new List<string>{ "Tires change, Breaks Change, Oil change" } },
            new OrderAppDTO { Id=8, MechanicName="Robert", MechanicSurname="Winnicki", CustomerName="Adam", CustomerSurname="Sanchez", IsFinished=true, ServiceList = new List<string>{ "Tires change, Breaks Change, Oil change" } },
            new OrderAppDTO { Id=9, MechanicName="Edvardo", MechanicSurname="Sanchez", CustomerName="Edvardo", CustomerSurname="Sanchez", IsFinished=true, ServiceList = new List<string>{ "Tires change, Breaks Change, Oil change" } },
            new OrderAppDTO { Id=10, MechanicName="Orlando", MechanicSurname="Merloni", CustomerName="Edvardo", CustomerSurname="Sanchez", IsFinished=false, ServiceList = new List<string>{ "Tires change, Breaks Change, Oil change" } },
            new OrderAppDTO { Id=11, MechanicName="Robert", MechanicSurname="Mike", CustomerName="Edvardo", CustomerSurname="Sanchez", IsFinished=false, ServiceList = new List<string>{ "Tires change, Breaks Change, Oil change" } },
            new OrderAppDTO { Id=12, MechanicName="Jacob", MechanicSurname="Samochodowy", CustomerName="Edvardo", CustomerSurname="Sanchez", IsFinished=true, ServiceList = new List<string>{ "Tires change, Breaks Change, Oil change" } },

        };
        public static ServiceDTO[] servicesDTO = new ServiceDTO[] { new ServiceDTO { ServiceId = 1, Name = "Oil change", Price = 20.00 }, new ServiceDTO { ServiceId = 2, Name = "Filter Change", Price = 15.25 }, new ServiceDTO { ServiceId = 3, Name = "Breaks Change", Price = 19.99 }, new ServiceDTO { ServiceId = 4, Name = "Tires change", Price = 9.99 }, new ServiceDTO { ServiceId = 4, Name = "Diagnostics", Price = 20.99 } };


        public OrderAppDTO[] GetOrders()
        {
            return ordersDTO;
        }

        public OrderAppDTO GetOrder(int id)
        {
            return ordersDTO.FirstOrDefault(m => m.Id == id)!;
        }

        public void EditOrder(int id, bool isFinished)
        {
            ordersDTO.FirstOrDefault(m => m.Id == id)!.IsFinished = isFinished;
        }
        public void AddServicesToOrder(int id, List<int> services)
        {
            throw new NotImplementedException();
        }

        public ServiceDTO[] GetServices()
        {
            return servicesDTO;
        }

    }
}
